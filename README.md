# correios-cep-search-rest

Project made to give Correios's SOAP CEP Search webservice a RESTfull interface.

To use it, run the server and send a GET request to:

```
baseURL:8080/correios/{CEP}
```

Where CEP must comply with the following regex: [0-9]{5}-[0-9]{3}

## Running the server

To run the testing server, just download the pom dependencies and run the src\main\java\br\com\correios\cep\server\Servidor.java file on your preferred environment.
