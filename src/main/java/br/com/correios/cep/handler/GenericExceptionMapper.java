package br.com.correios.cep.handler;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import br.com.correios.cep.model.ErrorModel;

public class GenericExceptionMapper implements ExceptionMapper<Exception> {

	@Override
	public Response toResponse(Exception exception) {
		return Response.status(500).entity(new ErrorModel("Erro inesperado no servidor.")).build();
	}

}
