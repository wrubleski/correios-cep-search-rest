package br.com.correios.cep.handler;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import br.com.correios.bsb.sigep.master.bean.cliente.SQLException_Exception;
import br.com.correios.cep.model.ErrorModel;

public class SQLException_ExceptionMapper implements ExceptionMapper<SQLException_Exception> {

	@Override
	public Response toResponse(SQLException_Exception exception) {
		return Response.status(500).entity(new ErrorModel("Erro inesperado no servidor.")).build();
	}
}
