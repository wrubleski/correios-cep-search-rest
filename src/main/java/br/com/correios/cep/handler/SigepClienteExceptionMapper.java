package br.com.correios.cep.handler;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import br.com.correios.bsb.sigep.master.bean.cliente.SigepClienteException;
import br.com.correios.cep.model.ErrorModel;

public class SigepClienteExceptionMapper implements ExceptionMapper<SigepClienteException> {

	@Override
	public Response toResponse(SigepClienteException exception) {
		return Response.status(404).entity(new ErrorModel("CEP n�o encontrado.")).build();
	}

}
