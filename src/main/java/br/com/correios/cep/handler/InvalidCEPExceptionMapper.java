package br.com.correios.cep.handler;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import br.com.correios.cep.model.ErrorModel;
import br.com.correios.cep.model.InvalidCEPException;

public class InvalidCEPExceptionMapper implements ExceptionMapper<InvalidCEPException> {

	@Override
	public Response toResponse(InvalidCEPException exception) {
		return Response.status(400).entity(new ErrorModel(exception.getMessage())).build();
	}

}
