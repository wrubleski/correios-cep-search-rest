package br.com.correios.cep.handler;

import br.com.correios.bsb.sigep.master.bean.cliente.AtendeCliente;
import br.com.correios.bsb.sigep.master.bean.cliente.AtendeClienteService;
import br.com.correios.bsb.sigep.master.bean.cliente.EnderecoERP;
import br.com.correios.bsb.sigep.master.bean.cliente.SQLException_Exception;
import br.com.correios.bsb.sigep.master.bean.cliente.SigepClienteException;
import br.com.correios.cep.model.InvalidCEPException;

public class CEPService {

	public EnderecoERP search(String CEP) throws SQLException_Exception, SigepClienteException {
		if (!CEP.matches("[0-9]{5}-[0-9]{3}"))
			throw new InvalidCEPException("CEP invalido.");

		AtendeClienteService servico = new AtendeClienteService();
		AtendeCliente proxi = servico.getAtendeClientePort();

		EnderecoERP consultaCEP = null;

		consultaCEP = proxi.consultaCEP(CEP);

		return consultaCEP;
	}
}
