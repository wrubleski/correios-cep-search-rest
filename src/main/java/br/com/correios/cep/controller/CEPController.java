package br.com.correios.cep.controller;

import javax.ws.rs.core.Response;

import br.com.correios.bsb.sigep.master.bean.cliente.EnderecoERP;
import br.com.correios.bsb.sigep.master.bean.cliente.SQLException_Exception;
import br.com.correios.bsb.sigep.master.bean.cliente.SigepClienteException;
import br.com.correios.cep.handler.CEPService;

public class CEPController {
	private CEPService service = new CEPService();

	public Response root_CEP_getRequest(String CEP) throws SQLException_Exception, SigepClienteException {

		EnderecoERP endereco = service.search(CEP);
		return Response.status(200).entity(endereco).build();

	}
}
