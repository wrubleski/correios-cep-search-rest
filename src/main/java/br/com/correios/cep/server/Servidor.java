package br.com.correios.cep.server;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;

import br.com.correios.cep.handler.GenericExceptionMapper;
import br.com.correios.cep.handler.InvalidCEPExceptionMapper;
import br.com.correios.cep.handler.SQLException_ExceptionMapper;
import br.com.correios.cep.handler.SigepClienteExceptionMapper;

public class Servidor {

	public static void main(String[] args) {
		try {
			URI uri = UriBuilder.fromUri("http://localhost").port(8080).build();
			ResourceConfig config = new ResourceConfig();
			config.register(JacksonFeature.class);
			config.register(InvalidCEPExceptionMapper.class);
			config.register(SQLException_ExceptionMapper.class);
			config.register(SigepClienteExceptionMapper.class);
			config.register(GenericExceptionMapper.class);
			config.packages("br.com.correios.cep.api");
			GrizzlyHttpServerFactory.createHttpServer(uri, config);
			System.out.println("Listening on port 8080");
		} catch (Exception e) {
			System.out.println(e);
			System.out.println("Erro na execucao: " + e.getMessage());
		}
	}
}
