package br.com.correios.cep.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import br.com.correios.bsb.sigep.master.bean.cliente.EnderecoERP;
import br.com.correios.bsb.sigep.master.bean.cliente.SQLException_Exception;
import br.com.correios.bsb.sigep.master.bean.cliente.SigepClienteException;
import br.com.correios.cep.controller.CEPController;
import br.com.correios.cep.model.ErrorModel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Path("/correio/{CEP}")
@Api(description = "the correio API")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJAXRSSpecServerCodegen", date = "2021-06-17T15:27:39.303-03:00[America/Sao_Paulo]")
public class CorreioApi {

	private CEPController controller = new CEPController();

	@GET
	@Produces({ "application/json" })
	@ApiOperation(value = "", notes = "Retorna as informa��es sobre o CEP", response = EnderecoERP.class, tags = {
			"CEP" })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Busca efetuada com sucesso", response = EnderecoERP.class),
			@ApiResponse(code = 400, message = "CEP no formato inv�lido", response = ErrorModel.class),
			@ApiResponse(code = 404, message = "CEP n�o encontado", response = ErrorModel.class),
			@ApiResponse(code = 500, message = "Erro inesperado", response = ErrorModel.class) })
	public Response buscaCEP(@PathParam("CEP") @ApiParam("CEP a ser buscado") String CEP)
			throws SQLException_Exception, SigepClienteException {
		return controller.root_CEP_getRequest(CEP);
	}
}
