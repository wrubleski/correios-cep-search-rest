package br.com.correios.cep.model;

public class CorreiosClientException extends RuntimeException {
	public CorreiosClientException(String message) {
		super(message);
	}
}
