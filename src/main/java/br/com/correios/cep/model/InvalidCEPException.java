package br.com.correios.cep.model;

public class InvalidCEPException extends RuntimeException {
	public InvalidCEPException(String message) {
		super(message);
	}
}
